import cors from 'cors';
import express from 'express';
import { WebSocketServer } from 'ws';

import { router as worldRouter } from './world';

console.log('== STARTING REFERENDUM ==');

// HTTP Server
const app = express();

app.use(express.json({ strict: false }));
app.use(cors());

app.use('/world', worldRouter);

const port = 8080;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

// WebSocket Server
let idCounter = 0;

const wss = new WebSocketServer({ port: 8081 });

wss.on('connection', ws => {
  const id = ++idCounter;

  console.log('Received connection:', id);

  ws.send(JSON.stringify({ type: 'connection', id }));

  ws.on('message', (data) => {
    const json = data.toString();
    const message = JSON.parse(json);
    console.log('onMessage', message);
  });

  ws.on('close', () => {
    console.log('Lost connection', id);
  });
});
