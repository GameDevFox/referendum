import { WebSocket } from "ws";

import { Socket } from "@referendum/common";

export const WSSocket = (ws: WebSocket) => {
  const result: Socket = {
    send: (message: any) => {
      const json = JSON.stringify(message);
      ws.send(json);
    },
    onMessage: () => {},

    close: () => ws.close(),
    onClose: () => {},
  };

  ws.on('message', (data) => {
    const json = data.toString();
    const message = JSON.parse(json);

    result.onMessage(message);
  });

  ws.on('close', () => result.onClose());

  return result;
};
