import { Router } from "express";
import { World } from "@referendum/common";

let worldId = 0;
let worlds: { [key: number]: World } = {};

const createWorld = ({ name }: { name: string }) => {
  const id = ++worldId;

  const world = { id, name };
  worlds[id] = world;

  return world;
};

createWorld({ name: 'Eden' });
createWorld({ name: 'Zion' });
createWorld({ name: 'Avalon' });

const router = Router();

router.get('/', (req, res) => res.send(Object.values(worlds)));

router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id);

  res.send(worlds[id]);
});

router.post('/', (req, res) => {
  const { name } = req.body;

  const world = createWorld({ name });

  res.send(world);
});

router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id);

  delete worlds[id];

  res.sendStatus(200);
});

export { router };
