import { SocketPair } from './socket-pair';
import { expectCalls } from './test-utils';

describe('SocketPair', () => {
  it('should work', async () => {
    const { a, b, sync } = SocketPair();

    a.onMessage = jest.fn();
    b.onMessage = jest.fn();

    a.send({ first: 'time' }); await sync();
    expectCalls(b.onMessage)([{ first: "time" }]);

    b.send({ another: 'one' }); await sync();
    expectCalls(a.onMessage)([{ another: 'one' }]);

    a.send("A string"); await sync();
    expectCalls(b.onMessage)(["A string"]);

    a.send({ final: 'message' }); await sync();
    expectCalls(b.onMessage)([{ final: 'message' }]);

    expectCalls(a.onMessage)();
  });
})
