export interface Socket {
  send: (message: any) => void,
  onMessage: (message: any) => void,

  close: () => void,
  onClose: () => void,
}
