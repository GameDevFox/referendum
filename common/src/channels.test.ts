import { fromSocket } from './channels';
import { SocketPair } from "./socket-pair";

describe('Channels', () => {
  it('should work', async () => {
    const { a, b, sync } = SocketPair();

    const channelsA = fromSocket(a);
    const channelsB = fromSocket(b);

    const calls = jest.fn();
    let myChannel;

    channelsB.onChannel = channel => {
      myChannel = channel;
      calls('onChannel', channel);

      channel.onMessage = message => {
        calls('channelB', message);
        channel.send('Back at ya!');
      };
    };

    const alpha = await channelsA.open('alpha');

    alpha.onMessage = message => {
      calls('channelA', message);
    };

    alpha.send("Hello World");

    await sync();

    expect(calls.mock.calls).toEqual([
      ['onChannel', myChannel],
      ['channelB', 'Hello World'],
      ['channelA', 'Back at ya!'],
    ]);
  });

  xit('channels can be closed', () => {});

  xit('channels can be rejected', () => {});
});
