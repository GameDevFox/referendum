export const doAsync = (fn: () => void) => new Promise<void>((resolve) => {
  setTimeout(() => {
    fn();
    resolve();
  }, 0);
});

export const noOp: any = () => {};

export const times = (count: number, fn: any) => {
  const result = [];

  for(let i=0; i<count; i++) {
    const value = fn(i);
    result.push(value);
  }

  return result;
};
