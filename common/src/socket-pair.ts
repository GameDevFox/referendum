import { doAsync } from ".";
import { Socket } from "./socket";

export const SocketPair = () => {
  const promises: Promise<void>[] = [];

  const a: any = {
    onMessage: () => {},
  };

  const b: any = {
    send: (message: any) => {
      const promise = doAsync(() => a.onMessage(message));
      promises.push(promise);
    },
    onMessage: () => {},
  };

  a.send = (message: any) => {
    const promise = doAsync(() => b.onMessage(message));
    promises.push(promise);
  }

  const sync = async () => {
    while(promises.length) {
      const currentPromises = promises.splice(0, promises.length);
      await Promise.all(currentPromises);
    }
  };

  return { a, b, sync };
};
