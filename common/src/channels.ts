import { Socket } from "./socket";

type PromiseResolve = (value: Channel | null | PromiseLike<Channel | null>) => void;

interface Channel extends Socket {
  id: any;
  accepted: boolean;
  resolve: PromiseResolve;
}

const getChannelId = (message: any) => {
  const { channelId } = message;
  if(!channelId)
    throw new Error(
      "Message doesn't have `channelId`: " +
      message
    );

  return channelId;
};

interface Channels extends Socket {
  open: (channelId: any) => Promise<Channel | null>;
  onChannel: (channel: Channel) => any;
}

export const Channels = () => {
  const result = {} as Channels;

  const channels: Record<any, Channel> = {};

  const getChannel = (channelId: any) => {
    const channel = channels[channelId];

    if(!channel)
      throw new Error(
        "No channel found for `channelId`: " +
        channelId
      );

    return channel;
  };

  const buildChannel = (channelId: any, resolve: PromiseResolve): Channel => ({
    id: channelId,
    accepted: false,
    resolve,

    send: (message: any) => result.onMessage({ channelId, message }),
    onMessage: (message: any) => {
      throw new Error(
        "Message sent to channel that wasn't accepted: " +
        message
      )
    },

    close: () => {},
    onClose: () => {},
  });

  // Socket side
  result.onMessage = () => {};

  result.send = message => {
    const channelId = getChannelId(message);

    if('message' in message) {
      const channel = getChannel(channelId);
      channel.onMessage(message.message);
    } else if('accepted' in message) {
      const channel = getChannel(channelId);

      const { accepted } = message;

      if(accepted)
        channel.accepted = accepted;
      else
        delete channels[channelId];

      channel.resolve(accepted ? channel : null);
    } else {
      // New channel
      (async () => {
        const channel = await buildChannel(channelId, () => {});

        const accept = await result.onChannel(channel);
        const accepted = accept !== false;

        if(accepted) {
          channel.accepted = true;
          channels[channelId] = channel;
        }

        result.onMessage({ channelId, accepted });
      })();
    }
  };

  // Channel side
  result.open = channelId => {
    return new Promise<Channel | null>((resolve) => {
      const channel: Channel = buildChannel(channelId, resolve);
      channels[channelId] = channel;

      result.onMessage({ channelId });
    });
  };

  result.onChannel = () => false;

  return result;
};

export const fromSocket = (socket: Socket) => {
  const channels = Channels();

  socket.onMessage = channels.send;
  channels.onMessage = socket.send;

  return channels;
}
