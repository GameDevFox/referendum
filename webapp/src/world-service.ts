import { createContext, useContext, useEffect } from "react";

import { Socket } from "./socket";

interface Message {
  type: string;
};

interface WorldMessage extends Message {
  worldId: number;
}

interface ConnectMessage extends WorldMessage {
  type: 'connect';
}

interface DisonnectMessage extends WorldMessage {
  type: 'disconnect';
}

export interface WorldServiceType {
  readonly connect: (worldId: number) => void,
  readonly disconnect: (worldId: number) => void,
}

export const WorldService = async (url: string) => {
  const socket = await Socket(url)
  socket.onMessage = (message) => {
    console.log('onMessage', message);
  };

  const connect = (worldId: number) => {
    const message: ConnectMessage = { type: 'connect', worldId };
    console.log('Send Connect Message:', message);
    socket.send(message);
  };

  const disconnect = (worldId: number) => {
    const message: DisonnectMessage = { type: 'disconnect', worldId };
    console.log('Send Disconnect Message:', message);
    socket.send(message);
  };

  return { connect, disconnect };
};

export const WorldContext = createContext<WorldServiceType | null>(null);

export const useWorld = (worldId: number) => {
  const worldService = useContext(WorldContext);

  if(!worldService)
    throw new Error('worldService not set in WorldProvider');

  useEffect(() => {
    worldService.connect(worldId);

    return () => worldService.disconnect(worldId);
  }, [worldService, worldId]);
};
