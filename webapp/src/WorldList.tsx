import { useState } from "react";
import { Button, Icon, Input, Table } from "semantic-ui-react";

import { World } from "@referendum/common";

import { worldResource } from "./services";
import { useResource } from "./useResource";

import { SimpleTable } from "./SimpleTable";
import { Link } from "react-router-dom";

export const WorldList = () => {
  const [name, setName] = useState<string>('');

  const { items: worlds, create, destroy } = useResource<World>(worldResource);

  const onCreateClick = async () => {
    if(name.trim() === '')
      return;

    create({ name });

    setName('');
  };

  return (
    <div className="world-list">
      <h1>Worlds</h1>

      <Input
        action={{
          icon: 'plus',
          content: 'Create',
          primary: true,
          onClick: onCreateClick,
        }}
        placeholder="Name"
        value={name}
        onChange={e => setName(e.currentTarget.value)}
      />

      <SimpleTable striped value={worlds}
        headers={['', 'Name', 'Delete']}
        renderRow={({ id, name }) => (
          <Table.Row key={id}>
            <Table.Cell collapsing>
              <Link to={`/world/${id}`}>
              <Button positive icon>
                <Icon name="play"/>
              </Button>
              </Link>
            </Table.Cell>
            <Table.Cell>{name}</Table.Cell>
            <Table.Cell collapsing>
              <Button negative icon onClick={() => destroy(id)}>
                <Icon name="trash"/>
              </Button>
            </Table.Cell>
          </Table.Row>
        )}
      />
    </div>
  );
}
