import { Socket } from '@referendum/common';

export const BrowserSocket = (url: string) => new Promise<Socket>((resolve) => {
  const webSocket = new WebSocket(url);

  const result: Socket = {
    send: message => {
      const json = JSON.stringify(message);
      webSocket.send(json);
    },
    onMessage: () => {},

    close: () => webSocket.close(),
    onClose: () => {},
  };

  webSocket.addEventListener('open', () => resolve(result));

  webSocket.addEventListener('message', event => {
    const message = JSON.parse(event.data);
    result.onMessage(message);
  });

  webSocket.addEventListener('close', () => result.onClose());
});
