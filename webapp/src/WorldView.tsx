import { useEffect, useState } from "react";
import { useRouteMatch } from "react-router";
import { Link } from "react-router-dom";
import { Button, Icon } from "semantic-ui-react";

import { World } from "@referendum/common";

import { getWorld } from "./services";
import { useWorld } from "./world-service";

interface Params {
  id: string;
}

export const WorldView = () => {
  const { params } = useRouteMatch<Params>();
  const worldId = parseInt(params.id);

  useWorld(worldId);

  const [world, setWorld] = useState<World | null>(null)

  useEffect(() => {
    getWorld(worldId).then(setWorld);
  }, [worldId]);

  return (
    <div className="world">
      <Link to="/">
        <Button icon labelPosition="left">
          <Icon name="angle double left"/>
          Back
        </Button>
      </Link>

      <h1>{world?.name}</h1>

      <pre>
        {JSON.stringify(world, null, 2)}
      </pre>
    </div>
  );
};
