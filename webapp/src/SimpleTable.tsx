import { ReactNode } from "react";
import { Table, TableProps } from "semantic-ui-react";

interface SimpleTableProps<T> extends TableProps {
    value: T[];
    headers: string[];
    renderRow: (item: T) => ReactNode;
  };

export const SimpleTable = <T,>(props: SimpleTableProps<T>) => {
  const { value, headers, renderRow, ...otherProps } = props;

  return (
    <Table {...otherProps}>
      <Table.Header>
        <Table.Row>
          {headers.map(header => (
            <Table.HeaderCell key={header}>{header}</Table.HeaderCell>
          ))}
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {value.map(renderRow)}
      </Table.Body>
    </Table>
  );
};
