import { World } from '@referendum/common';
import axios from 'axios';

const serviceURL = 'http://localhost:8080';

export const createWorld = ({ name }: Partial<World>) => {
  return axios.post(`${serviceURL}/world`, { name });
};
export const getWorlds = () => axios.get<World[]>(`${serviceURL}/world`).then(res => res.data);
export const getWorld = (id: number) => axios.get<World>(`${serviceURL}/world/${id}`).then(res => res.data);
export const deleteWorld = (id: number) => axios.delete(`${serviceURL}/world/${id}`);

export const worldResource = {
  create: createWorld,
  get: getWorlds,
  destroy: deleteWorld,
};
