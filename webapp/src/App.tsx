import { useEffect, useState } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import styled from 'styled-components';

import { WorldService, WorldServiceType } from './world-service';

import { WorldList } from './WorldList';
import { WorldProvider } from './WorldProvider';
import { WorldView } from './WorldView';

const Style = styled.div`
  padding: 16px;

  min-height: 100vh;
  background-color: #282c34;

  color: white
`;

export const App = () => {
  const [worldService, setWorldService] = useState<WorldServiceType | null>(null);

  useEffect(() => {
    WorldService('ws://localhost:8081').then(setWorldService);
  }, []);

  return worldService && (
    <WorldProvider worldService={worldService}>
      <Style className="app">
        <Switch>
          <Route exact path="/" component={WorldList}/>
          <Route path="/world/:id" component={WorldView}/>
          <Redirect to="/"/>
        </Switch>
      </Style>
    </WorldProvider>
  );
};
