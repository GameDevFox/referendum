import { ReactNode } from "react";
import { WorldContext, WorldServiceType } from "./world-service";

interface Props {
  worldService: WorldServiceType;
  children?: ReactNode;
}

export const WorldProvider = ({ worldService, children }: Props) => (
  <WorldContext.Provider value={worldService}>
    {children}
  </WorldContext.Provider>
);
