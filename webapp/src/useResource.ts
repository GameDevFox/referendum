import { useEffect, useState } from "react";

export interface Resource<T> {
  create: (item: Partial<T>) => void;
  get: () => Promise<T[]>;
  destroy: (id: number) => void;
};

export const useResource = <T,>({ create, get, destroy }: Resource<T>) => {
  const [items, setItems] = useState<T[]>([]);

  const loadItems = () => get().then(items => setItems(items as T[]));

  useEffect(() => {
    loadItems();
  }, []); /* eslint-disable-line react-hooks/exhaustive-deps */

  const doCreate = async (item: Partial<T>) => {
    await create(item);
    loadItems();
  };

  const doDestroy = async (id: number) => {
    await destroy(id);
    loadItems();
  };

  return { items, create: doCreate, destroy: doDestroy }
};
